%% Performance plots for Bayesian Logistic Regression.
% Compare Adaptive HMC vs NUTS, and Adaptive RMHMC vs RMHMC for Bayesian
% Logistic regression.
% This file produces figure 2 in the paper.
%
% Shakir and Ziyu, February 2013

%%

% Print PDF images
makePDF = 0;

% Seeds and data sets used
seedList = [1 50 500 2012 1000:1000:5000];
dataList = {'australian','german','heart','pima','ripley'};
algoList = {'AHMC', 'NUTS', 'ARMHMC', 'RMHMC'};
savList = {'bayesLogRegAHMC', 'bayesLogReg', 'bayesLogRegARMHMC', 'bayesLogRegRMHMC'}; % matches length of algoList - names saved differently
ixGroups = {[1 2],[3 4]};

% File name for mat file and pdf output
dataDir = '.';
outDir = 'plots';
savNames = {'plotData_AHMCvsNUTS_BLRall.mat','plotData_ARMHMCvsRMHMC_BLRall.mat'};
outNames = {'essL_AHMCvsNUTS_BLRall','essL_ARMHMCvsRMHMC_BLRall'};

for q = 1:length(savNames)
    savName = savNames{q};
    outName = outNames{q};
    ix = ixGroups{q};
    %% Load Data
    %
    % Plotting file: *plotLGC.m*
    
    if ~exist(savName,'file')
        for i = 1:2
            for j = 1:length(seedList)
                for k = 1:length(dataList)
                    disp([i j k])
                    fname = sprintf('%s/%s/%s_%s_%d.mat',dataDir,algoList{ix(i)},...
                        savList{ix(i)},dataList{k},seedList(j))
                    dat = load(fname);
                    minESS(i,j,k) = dat.ESS.minESS;
                    medESS(i,j,k) = dat.ESS.medESS;
                    maxESS(i,j,k) = dat.ESS.maxESS;
                    Lsteps(i,j,k) = dat.total_RandomStep;
                end;
            end;
        end;
        save(savName,'minESS', 'medESS','maxESS','Lsteps');
    else
        fprintf('Loading Data ...\n');
        load(savName);
    end;
    
    %% Show plots
    % Box plots compare the  performance, showing the that in most cases
    % the adaptive methods have better (higher) ESS/L. The two plots below are: 
    %  * AHMC vs NUTS 
    %  * ARMHMC vs RMHMC
    
    figure;
    pause(0.01); jf=get(gcf,'JavaFrame'); set(jf,'Maximized',1);
     
    subplot(1,3,1);
    labs = {'Australian','German','Heart','Pima','Ripley'}; % caps for plots
    aboxplot(minESS./Lsteps*5000,'labels',labs)
    set(gca,'FontSize',14);
    grid on;
    title('Minimum ESS/L','FontSize',14, 'FontWeight','bold');
    legend(algoList(ix),'FontSize',14, 'FontWeight','bold','location','NorthWest');
    axis square;
    
    subplot(1,3,2);
    aboxplot(medESS./Lsteps*5000,'labels',labs)
    set(gca,'FontSize',14);
    grid on;
    title('Median ESS/L','FontSize',14, 'FontWeight','bold');
    axis square;
    
    subplot(1,3,3);
    aboxplot(maxESS./Lsteps*5000,'labels',labs)
    set(gca,'FontSize',14);
    grid on;
    title('Maximum ESS/L','FontSize',14, 'FontWeight','bold');
    axis square;
    
    if makePDF
        printPDF(outName,outDir);
    end;
end;

