%% Adaptive Hamiltonian and Riemann Manifold Monte Carlo
%
% Ziyu Wang, Shakir Mohamed and Nando de Freitas

%% Description
% This code provides an implementation of adaptive MCMC algorithms for Hamiltonian/Hybrid Monte Carlo (AHMC)and Riemannian Manifold Monte Carlo (ARMHMC). The algorithms remove the need for manual and expert tuning of the hyperparameters governing the performance of these samplers, allowing for more efficient sampling and explopration of the posterior distribution of many statistical models.
%
% We demonstrate the application of AHMC in sampling from a bivariate Gaussian distribution,
% Bayesian Logistic regression, a stochastic
% volitility model, and a log Gaussian cox point process. We provide the code for these models, as well as the code to reproduce all the plots in the corresponding paper.
%
% Ziyu and Shakir, 2013.


%% Download the code
% The code was written and tested using Matlab 2012a (7.14)
% 
% * 8 May 2013: Initial release. Download <ahmc.tar here>. 

%% 1. Bivariate Gaussian distribution
% The results for this experiment, see <ahmc_gaussian_demo.html Gaussian results>.

%% 2. Bayesian Logistic regression
% The results for this experiment, see <demo_ahmc_BLR.html BLR results>.

%% 3. Stochastic volitility model
% The results for this experiment, see <demo_ahmc_stochVol.html SV results>.

%% 4. Log Gaussian cox point process
% The results for this experiment, see <demo_ahmc_LGC.html LGC results>.

%% Experiments in Chapter
% All the plots in the paper can be reproduced. 
% Download code and view instructions and images <http://www.cs.ubc.ca/~ziyuw/ahmc/plotMain.html here>

%% Corresponding paper
% Z. Wang, S. Mohamed and N. de Freitas, *Adaptive Hamiltonian and Riemann
% Monte Carlo Samplers*. International Conference on Machine Learning (ICML),
% 2013.

%% Miscellaneous
% This code may have bugs. If you find any errors, please let us know.
