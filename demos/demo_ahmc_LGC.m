%% Demo of adaptive HMC for a Log Guassian Poisson Point Process
% Shows application of AHMC on high dimensional data. 
%
% Ziyu and Shakir, 2013.

%% Data set and seed
% We use a synthetic data set.
seed = 1;

%% Run AHMC for LGC Model
% Note: This takes a long time to run.
out = LGC_HMC_LV(seed);
samples = out.saved; %samples after burnin

%% Analysis
% Refer to analysis of results in the paper:
% Z. Wang, S. Mohamed and N. de Freitas, *Adaptive Hamiltonian and Riemann
% Monte Carlo Samplers*. <http://arxiv.org/abs/1302.6182 arXiv Preprint>, 2013.
% 
% The results for this experiment, see <plotMain.html Plot Result>.



