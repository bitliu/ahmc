%% AHMC sampling for a 2D Gaussian
% simple example to show trajectory and sampling behaviour of adaptive HMC
% for a Gaussian.
%
% Ziyu and Shakir, 2013

%% Specify Gaussian model
% Use a bivriate Gaussian with correlation = -0.4

target_sigma = [1, 0.99; 0.99, 1];
target_mean = [3; 5];
func = inline('transpose(x-target_mean)*inv(target_sigma)*(x-target_mean)/2');
func = @(x)func(target_mean, target_sigma, x);
gradient  = inline('inv(target_sigma)*(x-target_mean)');
gradient = @(x)gradient(target_mean, target_sigma, x);

max_iter = 5000; 
burnIn = 1000;
dims = 2;
sigmaCholR = chol(target_sigma, 'lower');
bounds = [0.01, 0.4; 1, 50];

%% Run AHMC
% Uses the gradaient and function value of the negative log likelihood.
[out] = ahmc(max_iter, burnIn, bounds, gradient, func, dims);
qs = out.saved;

%% Plot samples
C = acorrtime(qs);
C = acorr(qs, max_iter - 1);    
figure(1); clf;
xx = qs(:,1);
yy = qs(:,2);
scatter(xx, yy, 4);
p = plot(xx(:), yy(:),'x');
set(p,'Color','red','LineWidth',1)
set(gca,'FontSize',12);
hold on;
x1 = 0:.01:6; x2 = 2:.01:8;
[X,Y] = meshgrid(x1,x2);
F = mvnpdf([X(:) Y(:)], target_mean', target_sigma);
F = reshape(F,length(x2),length(x1));
contour(x1, x2, F, 5, 'LineWidth',2);
xlabel('Dimension 1', 'FontSize',14,'FontWeight','bold')
ylabel('Dimension 2', 'FontSize',14,'FontWeight','bold')
title('Samples from a 2D Gaussian', 'FontSize',14,'FontWeight','bold')
grid on; axis square;

%%  Plot auto-correlation
figure(2); clf;
plot(C(1:100, 1), 'LineWidth',2);
grid on;
set(gca,'Ylim',[-0.2, 1])
xlim([0 100]);
set(gca,'FontSize',12);
xlabel('Lag', 'fontsize', 14, 'FontWeight','bold');
ylabel('Autocorrelation', 'FontSize',14,'FontWeight','bold')
title('Autocorrelation plot', 'FontSize',14,'FontWeight','bold')


